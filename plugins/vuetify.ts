
import { createVuetify } from 'vuetify';
import 'vuetify/styles'; // pre-build css styles

/* Add all components and directives, for dev & prototyping only. */
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';
/* Add build-in icon used internally in various components */
/* Described in https://next.vuetifyjs.com/en/features/icon-fonts/ */
import { mdi, aliases } from 'vuetify/iconsets/mdi';
import "@mdi/font/css/materialdesignicons.css";
const myCustomLightTheme = {
  dark: false,
  colors: {
    current: "#1E1E1E",
    primary: "#2F2F2F",
    accent: "#4C4C4C",
    secondary: "#9F957C",
    "gray-darken": "#242424",
    "gray-dark": "#45413E",
    "gray-light": "#949494",

  }
}

export default defineNuxtPlugin((nuxtApp) => {

  const vuetify = createVuetify({
    components,
    directives,
    theme: {
      defaultTheme: 'myCustomLightTheme',
      themes: {
        myCustomLightTheme,
      }
    },
    icons: {
      defaultSet: 'mdi',
      aliases,
      sets: {
        mdi,
      },
    },
    defaults: {
      global: {
        elevation: 0,

      },

    },
  });

  nuxtApp.vueApp.use(vuetify);

});
