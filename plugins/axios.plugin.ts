import axios from "axios";
export default defineNuxtPlugin((nuxtApp) => {
  const config = useRuntimeConfig()
  const router = useRouter()
  const { notif } = globalComposables()
  axios.defaults.baseURL = config.public.apiUrl,
    // make interceptors
    axios.interceptors.request.use(
      async (config) => {
        // add jwt token to every request with cookies
        // if (value) {
        //   config.headers.Authorization = 'Bearer ' +value

        // }
        return config
      },
      (error) => {
        // Do something with request error
        return Promise.reject(error)
      }
    )

  // interceptors response
  axios.interceptors.response.use(
    async (res) => {
      return res
    },
    (error) => {
      const statusCode = error.response?.status || null
      const msg =
        error?.response?.data?.message ||
        null
      if (statusCode === 401) {
        notif("error", msg, "Authorization",)


       

      }
      if (statusCode === 422) {
        notif("error", msg)


      }
      if (statusCode === 404) {
        notif("error", msg)

      }
      if (statusCode === 403) {
        notif("error", msg)

      }

      if (statusCode === 503) {
        notif("error", msg)

      }
      if (statusCode === 500) {
        notif("error", msg)

      }

      return Promise.reject(error)
    }
  )


  return {
    provide: {
      axios: axios,
    },

  };

});