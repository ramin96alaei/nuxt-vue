import { useNotification } from "@kyvg/vue3-notification";

export default function setup() {
    const { notify } = useNotification();

    const notif = (type: string, text: string, title?: string) => {
        return notify({
            type: type,
            title: title || '',
            text: text,
            duration: 5000,
        });

    }






    return { notif }
}

