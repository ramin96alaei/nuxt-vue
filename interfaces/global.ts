export interface IToken {
    access_token?: string,
    refresh_token?: string,
}
export interface IHeader {
    title: string,
    align: string,
    sortable: boolean,
    key: string,
}


export interface ILogin {
    password?: string,
    email?: string,
}

export interface IPagination {
    current_page?: number,
    last_page: number,
    per_page: number,
}
export interface IUser {

    id?: number | string,
    name?: string,
    email?: string,
    nationality?: string,
    last_name?: string,
    username?: string,
    score?: number | string,
    avatar?: string,
    wallet?: number | string,
    win_number?: number | string,
    lose_number?: number | string,
    skill_level?: number | string,
    sheba?: number | string,
    lang?: number | string,
    created_at?: number | string,
    updated_at?: number | string
    phone_number?: number | string

}

export interface IDevice {
    id?: string | number,
    title?: string,
    description?: string,
    image?: string,
    created_at?: string,
    updated_at?: string

}



export interface IChannel {
    id?: string | number,
    title?: string,
    description?: string,
    game_id?: string | number,
    devices?: [],
    created_at?: string,
    updated_at?: string,

}
export interface IGames {
    id?: string | number,
    title?: string,
    description?: string,
    image?: string,
    created_at?: string,
    updated_at?: string
    channels?: any[]

}

export interface ISponsor {
    id?: string | number,
    title?: string,
    max_bonus?: string | number,
    used_bonus?: string | number,
    image?: string,
    created_at?: string,
    updated_at?: string

}
export interface IRoom {
    id?: string,
    status?: string | number,
    price?: string | number,
    bonus?: string | number,
    max_bonus?: string | number,
    used_bonus?: string | number,
    description?: string,
    commission?: string,
    color?: string,
    limit?: string | number,
    used_limit?: string | number,
    shadow_option?: boolean,
    blur?: string | number,
    opacity?: string | number,
    created_at?: string,
    updated_at?: string

}
export interface ISelect {
    title: string | number;
    value: string | number;
}

export interface INationality {
    name: string
    code: string
}
export interface IGiftCardCategory {
    id: string,
    title: string,
    status: boolean | number,
    expire_date_time: string,
    extra_charge: string,
    is_block: boolean,
    created_at: string,
    updated_at: string,
}

export interface IGiftCard {
    id: number,
    is_block: boolean,
    is_archive: boolean,
    status: boolean | number,
    expire_date_time: string,
    gift_card_category: IGiftCardCategory,
    value: number,
    card_id: string,
    redeem_code: string,
    redeemed_date_time: string,
    user: string,
    created_at: string,
    updated_at: string

}
export interface ICatagoryCard {
    id: number,
    title: string,
    is_block: boolean,
    status: boolean | number,
    expire_date_time: string,
    extra_charge: string | number
    created_at: string,
    updated_at: string

}

export interface IPivot {
    role_id: string,
    permission_id: string
}
export interface IPermission {
    name: string,
    id: string,
    guard_name: string
    created_at: string,
    updated_at: string,
    pivot: IPivot
}
export interface IRole {
    name: string,
    id: string,
    guard_name: string
    created_at: string,
    updated_at: string,
    permissions: IPermission
}
export interface IRule {
    id: string,
    text: string,
    game_id: string,
    created_at: string,
    updated_at: string,
}

