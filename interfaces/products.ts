export interface IClmns {
    field: string;
    header: string;
}

export interface IProduct {
    categoryId: number;
    id: number;
    isPublished: boolean;
    status: string;
    statusId: number;
    title: string;
}

export interface IStatus {
    title: string;
    id: number;
}

export interface ICatagoryChildren {
    id: number;
    title: string;
}
export interface ICatagory {
    children?: ICatagoryChildren[];
    id: number;
    title: string;
}

export interface IDefaultValuesSttributes {
    id: number;
    value: string
}

export interface IAttributes {
    attrId: number;
    attrText: string;
    attrValueId: number;
    id: number;
    defaultValues?: IDefaultValuesSttributes[]
    valueText: string
}