const colors = require("tailwindcss/colors");
module.exports = {
  mode: "jit",
  prefix: "tw-",
  content: [
    "./**/*.{html,js}",
    "./layouts/*.{vue,js}",
    "./layouts/**/*.{vue,js}",
    "./components/*.{vue,js}",
    "./components/**/*.{vue,js}",
    "./pages/*.vue",
    "./app.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./*.{vue,js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  theme: {
    extend: {},
    colors: {
      transparent: "transparent",
      current: "#1E1E1E",
      primary: "#2F2F2F",
      accent: "#4C4C4C",
      
      secondary: "#9F957C",
      "gray-darken": "#242424",
      "gray-dark": "#45413E",
      "gray-light": "#949494",
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      red: colors.red,
      yellow: colors.amber,
      blue: colors.blue,
      indigo: colors.indigo,
      purple: colors.violet,
      pink: colors.pink,
      orange: colors.orange,
      amber: colors.amber,
      green: colors.green,
    },
  },

  variants: {
    extend: {},
  },
  plugins: [],
};
