// https://nuxt.com/docs/api/configuration/nuxt-config
// https://v3.nuxtjs.org/api/configuration/nuxt.config


export default defineNuxtConfig({


    css: ['vuetify/lib/styles/main.sass', '~/assets/index.scss'],
    app: {

        head: {
            link: [{ rel: 'icon', type: 'image/png', href: '/favicon.png' }]
        },
        pageTransition: { name: 'page', mode: 'out-in' }
    },
    postcss: {
        plugins: {
            tailwindcss: {},
            autoprefixer: {},
        },
    },
    build: {

        transpile: ['vuetify'],




    },
    vite: {
        define: {
            'process.env.DEBUG': true,
        },
    },


    imports: {
        dirs: [
            // ... or scan all modules within given directory 
            'composables/**']
    },
    modules: ['@pinia/nuxt'],

    runtimeConfig: {
        public: {
            apiUrl: process.env.API_URL,
            perfix: process.env.AUTH_COOKIE_PERFIX,
            secure: process.env.AUTH_COOKIE_SECURE,
            sameSite: process.env.APP_COOKIE_SAMESITE,
            domain: process.env.AUTH_COOKIE_DOMAIN
        }
    }

}) 